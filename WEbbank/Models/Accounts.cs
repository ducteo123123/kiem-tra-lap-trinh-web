﻿using System.ComponentModel.DataAnnotations;

namespace WEbbank.Models
{
    public class Accounts
    {
        [Key]
        public int AccountId { get; set; }
        public int CustomerId { get; set; }
        public List<Customer>? Customer { get; set; }
        [Required, StringLength(100)]
        public string AccountName { get; set; }
    }
}
