﻿using System.ComponentModel.DataAnnotations;

namespace WEbbank.Models
{
    public class Employees
    {
        [Key]
        public int EmployeeId { get; set; }
        [Required, StringLength(50)]
        public string FirstName { get; set; }
        [Required, StringLength(50)]
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
