﻿using System.ComponentModel.DataAnnotations;

namespace WEbbank.Models
{
    public class Reports
    {
        [Key]
        public int ReportId { get; set; }
        public int AccountId { get; set; }
        public Accounts? Accounts { get; set; }
        public int LogsId { get; set; }
        public Logs? Logs { get; set; }
        public int TransactionalId { get; set; }
        public Transactions? Transactions { get; set; }
        [Required]
        public string ReportName { get; set; }
        public DateTime? ReportDate { get; set; }
    }
}
