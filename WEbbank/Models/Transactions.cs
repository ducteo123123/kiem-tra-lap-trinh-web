﻿using System.ComponentModel.DataAnnotations;

namespace WEbbank.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionalId { get; set; }
        [Required]
        public string Name { get; set; }
        public int EmployeeId { get; set; }
        public Employees? Employees { get; set; }
        public int CustomerId { get; set; }
        public Customer? Customer { get; set; }
    }
}
